# Source this file for misc utilities

# Create an android boot image compatible initramfs from a directory
# $1: The source directory
# $2: The target file
function mkinitfs() {
    OUT=$(realpath $2)
    pushd $1
    find . -print0 | cpio --null --create --format=newc | gzip --best $OUT
    popd > /dev/null
}

MAKE="make O=.output/ ARCH=arm64 CC=/usr/bin/aarch64-linux-gnu-gcc CROSS_COMPILE=/usr/bin/aarch64-linux-gnu- -j13"
LOG=$(pwd)/log.txt

function print() {
    echo "$SCRIPT: $@" | tee log.txt
}

function die() {
    print $1
    exit 1
}

function log() {
    print "\$ $*"
    "$@" | tee log.txt
    [[ $? -ne 0 ]] && die "Command failed, exiting"
}