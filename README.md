# Bootswitcher
A small mainline linux based boot image to be chainloaded instead of a normal Android boot image.

Uses kexec to boot the users kernel of choice.

## To build

```
git clone https://gitlab.com/sdm845-mainline/bootswitcher --recurse-submodules
cd bootswitcher
./build.sh <device>
```

Run `./build.sh` for a list of available devices

## Supported devices

|Device|Notes|
|------|-----|
|OnePlus 6/T|Both use same kernel but require different device trees|

## Add your device

1. Get mainline booting with at least storage, framebuffer and touch support
2. Submit an MR [to the sdm845-linux kernel](https://gitlab.com/sdm845-mainline/sdm845-linux)
3. Make an MR to add a minimal defconfig and device config. [Example device config](https://gitlab.com/sdm845-mainline/bootswitcher/-/blob/master/devices/enchilada)

This process will improve as development matures, I hope :D