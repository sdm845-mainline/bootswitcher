#!/bin/bash

SCRIPT="$(basename $0)"
OUTDIR="$(pwd)/out"
INITRAMFS="$OUTDIR/ramdisk.cpio.gz"
OUTFILE="$OUTDIR/bootswitcher.img"
KERNELDIR=$(pwd)/kernel
INITRAMFSDIR=$(pwd)/initramfs
DEFCONFIG_TARGET="$KERNELDIR/arch/arm64/configs"

mkdir -p $OUTDIR

source utils.sh

if [ -z $1 ]; then
    print "You must build for a device"
    print "bootswitcher supports the following devices:"
    DEVS=$(find devices/ -maxdepth 1 -type f -exec basename {} \;)
    echo
    while IFS= read -r dev; do
        echo " * $dev"
    done <<< "$DEVS"
    echo
    print "e.g: '$SCRIPT enchilada'"
    exit 1
fi

DEVICE=$(pwd)/devices/$1

if [ ! -f $DEVICE]; then
    print "Could not find $DEVICE"
    exit 1
fi

print "Clearing log"
echo "" > log.txt

# Variables available
DEVICE_NAME=""
DEVICE_DEVICETREE=""
DEVICE_BOOTIMG_BASE=""
DEVICE_BOOTIMG_KERNEL=""
DEVICE_BOOTIMG_RAMDISK=""
DEVICE_BOOTIMG_SECOND=""
DEVICE_BOOTIMG_TAGS=""
DEVICE_KERNEL_DEFCONFIG=""

source $DEVICE
print "Building for device: $DEVICE_NAME"

DEFCONFIG_TARGET="$DEFCONFIG_TARGET/$DEVICE_KERNEL_DEFCONFIG"
print "Using defconfig: $DEVICE_KERNEL_DEFCONFIG"

cp devices/defconfigs/$DEVICE_KERNEL_DEFCONFIG $DEFCONFIG_TARGET
print "Cleaning up from previous builds"
log $MAKE -C $KERNELDIR clean
log $MAKE -C $KERNELDIR $DEVICE_KERNEL_DEFCONFIG
log $MAKE -C $KERNELDIR -s

print "Building OS picker"

log echo "NOP"

print "Generating initramfs: $INITRAMFS"

log mkinitfs $INITRAMFSDIR $INITRAMFS

print "Making boot image"

log mkbootimg \
    --base $DEVICE_BOOTIMG_BASE \
    --kernel_offset $DEVICE_BOOTIMG_KERNEL \
    --ramdisk_offset $DEVICE_BOOTIMG_RAMDISK \
    --tags_offset $DEVICE_BOOTIMG_TAGS \
    --pagesize 4096 \
    --second_offset $DEVICE_BOOTIMG_SECOND \
    --kernel /tmp/kernel-dtb -o $OUTFILE

print "Cleaning up"
log rm $DEFCONFIG_TARGET
log rm -r $KERNELDIR/.output

echo
echo "====="
echo
print "Boot switcher has been built to: $OUTFILE"