#!/bin/sh
IP=172.16.42.1
LOGS="/mnt/userdata/media/0/bootswitcher/logs"

setup_log() {
		echo "NOTE: All output from the initramfs gets redirected to :"
		echo "/init.log"

		# Start redirect
		#exec >/init.log 2>&1
		#echo "### Initramfs Debug Tool ###"
}

mount_proc_sys_dev() {
	# mdev
	mount -t proc -o nodev,noexec,nosuid proc /proc || echo "Couldn't mount /proc"
	mount -t sysfs -o nodev,noexec,nosuid sysfs /sys || echo "Couldn't mount /sys"

	mkdir /config
	mount -t  configfs -o nodev,noexec,nosuid configfs /config || echo "Couldn't mount /config"

	mkdir -p /dev/pts || echo "Couldn't create directory /dev/pts"
	mount -t devpts devpts /dev/pts || echo "Couldn't mount /dev/pts"

	mkdir /run
}

setup_mdev() {
	echo /sbin/mdev > /proc/sys/kernel/hotplug
	mdev -s
}

setup_usb_network() {
	# See: https://www.kernel.org/doc/Documentation/usb/gadget_configfs.txt
	CONFIGFS=/config/usb_gadget

	if ! [ -e "$CONFIGFS" ]; then
		echo "  /config/usb_gadget does not exist, skipping configfs usb gadget"
		return
	fi

	# Default values for USB-related deviceinfo variables
	usb_idVendor="${deviceinfo_usb_idVendor:-0x18D1}"   # default: Google Inc.
	usb_idProduct="${deviceinfo_usb_idProduct:-0xD001}" # default: Nexus 4 (fastboot)
	usb_serialnumber="${deviceinfo_usb_serialnumber:-postmarketOS}"
	usb_rndis_function="${deviceinfo_usb_rndis_function:-rndis.usb0}"

	echo "  Setting up an USB gadget through configfs"
	# Create an usb gadet configuration
	mkdir $CONFIGFS/g1 || echo "  Couldn't create $CONFIGFS/g1"
	echo "$usb_idVendor"  > "$CONFIGFS/g1/idVendor"
	echo "$usb_idProduct" > "$CONFIGFS/g1/idProduct"

	# Create english (0x409) strings
	mkdir $CONFIGFS/g1/strings/0x409 || echo "  Couldn't create $CONFIGFS/g1/strings/0x409"

	# shellcheck disable=SC2154
	echo "$deviceinfo_manufacturer" > "$CONFIGFS/g1/strings/0x409/manufacturer"
	echo "$usb_serialnumber"        > "$CONFIGFS/g1/strings/0x409/serialnumber"
	# shellcheck disable=SC2154
	echo "$deviceinfo_name"         > "$CONFIGFS/g1/strings/0x409/product"

	# Create rndis function. The function can be named differently in downstream kernels.
	mkdir $CONFIGFS/g1/functions/"$usb_rndis_function" \
		|| echo "  Couldn't create $CONFIGFS/g1/functions/$usb_rndis_function"

	# Create configuration instance for the gadget
	mkdir $CONFIGFS/g1/configs/c.1 \
		|| echo "  Couldn't create $CONFIGFS/g1/configs/c.1"
	mkdir $CONFIGFS/g1/configs/c.1/strings/0x409 \
		|| echo "  Couldn't create $CONFIGFS/g1/configs/c.1/strings/0x409"
	echo "rndis" > $CONFIGFS/g1/configs/c.1/strings/0x409/configuration \
		|| echo "  Couldn't write configration name"

	# Link the rndis instance to the configuration
	ln -s $CONFIGFS/g1/functions/"$usb_rndis_function" $CONFIGFS/g1/configs/c.1 \
		|| echo "  Couldn't symlink $usb_rndis_function"

	# Check if there's an USB Device Controller
	if [ -z "$(ls /sys/class/udc)" ]; then
		echo "  No USB Device Controller available"
		return
	fi

	# Link the gadget instance to an USB Device Controller. This activates the gadget.
	# See also: https://github.com/postmarketOS/pmbootstrap/issues/338
	# shellcheck disable=SC2005
	echo "$(ls /sys/class/udc)" > $CONFIGFS/g1/UDC || echo "  Couldn't write UDC"

}

start_udhcpd() {
	touch /etc/udhcpd.conf
	ifconfig usb0 "$IP"
}

start_telnetd() {
	mkdir -p /dev/shm
	mount -t tmpfs tmpfs /dev/shm
	telnetd -l /bin/sh
}

mount_userdata_partition() {
	mkdir /mnt/userdata
	mount /dev/sda13 /mnt/ || echo "Couldn't mount /dev/sda13!" > /dev/kmsg
}

log() {
	# Print messages on device screen
	echo "# $1" > /dev/kmsg
	eval $1 > /dev/kmsg

	# Save log to file on internal storage
	if [ ! -z "$2" ]; then
		echo "# $1" > "$LOGS/$2.log"
		eval $1 >> "$LOGS/$2.log" # e.g. "$LOGS/uname.log"
		echo -e "========================\n" >> "$LOGS/$2.log"
	fi
}

inject_loop() {
    INJ_DIR=/init-ctl
    INJ_STDIN=$INJ_DIR/stdin

    mkdir -p $INJ_DIR
    mkfifo $INJ_STDIN
    echo "This entire directory is for debugging init - it can safely be removed" > $INJ_DIR/README

    echo "########################## Beginning inject loop" > /dev/kmsg
    while : ; do
        while read IN; do
	    if [ "$IN" = "continue" ]; then break 2;fi
            $IN > /dev/kmsg
        done <$INJ_STDIN
    done
    rm -rf $INJ_DIR # Clean up if we exited nicely
    echo "########################## inject loop done"
}